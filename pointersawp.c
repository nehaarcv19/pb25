#include<stdio.h>
void sawp(int *a,int *b)
{
    int temp;
    temp=*a;
    *a=*b;
    *b=temp;
}
int main()
{
    int n1,n2;
    printf("\nEnter the two numbers: ");
    scanf("%d %d",&n1,&n2);
    printf("\nThe value of first and second number before function call:%d %d",n1,n2);
    swap(&n1,&n2);
    printf("\nThe value of first and second number after function call:%d %d",n1,n2);
    return 0;
}